<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Minmap" version="1.5.4" date="03/05/2011">
		<VersionSettings gameVersion="1.4.1" windowsVersion="0.2" savedVariablesVersion="0.2" />
		<Author name="Adalyn (Maintainer: Crestor)" email="" />
		<Description text="I'm back with a new minmap." />
		<Dependencies>
			<Dependency name="EATemplate_DefaultWindowSkin" />
			<Dependency name="EA_OverheadMapWindow" />
			<Dependency name="EA_ScenarioLobbyWindow" />
		</Dependencies>
		<Files>
			<File name="core.lua" />
			<File name="minimap.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="Minmap.Initialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="Minmap.Shutdown" />
		</OnShutdown>
		<SavedVariables>
			<SavedVariable name="MinmapSettings" />
		</SavedVariables>
		<WARInfo>
			<Categories>
				<Category name="MAP" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile> 