--[[Copyright (c) 2008 Brad Bates

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.]]--

Minmap = {}
local zoomlevelvar = 0.5
local windowname = "Minmap"
local map_windowname = windowname.."MapDisplay"
local filtername = windowname.."PinMenu"
local LeavingQueues = false
local ScaleOffset = 2/6

Minmap.INSTANCETYPE_SCENARIO = 3
Minmap.INSTANCETYPE_CITY = 4
-- LA HUGE PILE O' CRAP! --
	defaultsettings = {
		ver				= 7,
		zoomlevel		= SystemData.OverheadMap.MIN_ZOOM_LEVEL,
		worldzoom		= 1,
		mapscale		= 1,
		worldmapscale	= 1-ScaleOffset,
		mapPinFilters	= {
			[ SystemData.MapPips.KILL_COLLECTOR_QUEST_PENDING_NPC ]		= false,
			[ SystemData.MapPips.KILL_COLLECTOR_QUEST_COMPLETE_NPC ]	= false,
			[ SystemData.MapPips.STORE_NPC ]							= false,
			[ SystemData.MapPips.SCENARIO_GATEKEEPER_NPC ]				= false,
			[ SystemData.MapPips.VAULT_KEEPER_NPC ]					 	= false,
			[ SystemData.MapPips.BINDER_NPC ]							= false,
			[ SystemData.MapPips.GUILD_REGISTRAR_NPC ]					= false,
			[ SystemData.MapPips.HEALER_NPC ]							= true,
			[ SystemData.MapPips.MAILBOX ]								= true,
			[ SystemData.MapPips.MERCHANT_LASTNAME ]					= false,
			[ SystemData.MapPips.MERCHANT_DYE ]							= false,
			[ SystemData.MapPips.BANNER ]								= false,
			[ SystemData.MapPips.INFLUENCE_REWARDS_NPC ]				= false,
			[ SystemData.MapPips.INFLUENCE_REWARDS_PENDING_NPC ]		= false,
		},
		mapPinGutters	= {
			[ SystemData.MapPips.OBJECTIVE ]							= false,
			[ SystemData.MapPips.KEEP ]									= false,
			[ SystemData.MapPips.GROUP_MEMBER ]							= false,
			[ SystemData.MapPips.IMPORTANT_MONSTER ]					= false,
		},
	}
--
----------------------------------------------------------------
-- Map Pin Filter to Map Pin lookup table
----------------------------------------------------------------
Minmap.mapPinFilters =
{
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_WAYPOINT,			slice="Waypoint-Large",				scale=1.0,	pins={ SystemData.MapPips.QUEST_AREA, SystemData.MapPips.LIVE_EVENT_WAYPOINT }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_PUBLIC_QUEST,		slice="PQ-Large",					scale=1.0,	pins={ SystemData.MapPips.PUBLIC_QUEST }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_QUESTS,				slice="QuestCompleted-Gold",		scale=1.0,	pins={ SystemData.MapPips.QUEST_OFFER_NPC, SystemData.MapPips.REPEATABLE_QUEST_OFFER_NPC, SystemData.MapPips.LIVE_EVENT_QUEST_OFFER_NPC, SystemData.MapPips.QUEST_PENDING_NPC, SystemData.MapPips.QUEST_COMPLETE_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_KILL_COLLECTOR,		slice="KillCollector",				scale=1.0,	pins={ SystemData.MapPips.KILL_COLLECTOR_QUEST_PENDING_NPC, SystemData.MapPips.KILL_COLLECTOR_QUEST_COMPLETE_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_MERCHANT,			slice="NPC-Merchant",				scale=1.0,	pins={ SystemData.MapPips.STORE_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_TRAINERS,			slice="NPC-TrainerActive",			scale=1.0,	pins={ SystemData.MapPips.TRAINER_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_BINDER,				slice="NPC-Binder",					scale=1.1,	pins={ SystemData.MapPips.BINDER_NPC, SystemData.MapPips.INFLUENCE_REWARDS_NPC, SystemData.MapPips.INFLUENCE_REWARDS_PENDING_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_MAILBOX,				slice="Mail-Large",					scale=1.0,	pins={ SystemData.MapPips.MAILBOX }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_TRAVEL,				slice="NPC-Travel",					scale=1.0,	pins={ SystemData.MapPips.TRAVEL_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_HEALER,				slice="NPC-Healer-Large",			scale=1.0,	pins={ SystemData.MapPips.HEALER_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_VAULT,				slice="Vault",						scale=1.0,	pins={ SystemData.MapPips.VAULT_KEEPER_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_AUCTION,				slice="Auctioneer",					scale=1.0,	pins={ SystemData.MapPips.AUCTION_HOUSE_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_GUILD_REGISTRATION,	slice="NPC-GuildRegistrar-Large",	scale=1.0,	pins={ SystemData.MapPips.GUILD_REGISTRAR_NPC }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_LASTNAME,			slice="LastNames-Large",			scale=1.0,	pins={ SystemData.MapPips.MERCHANT_LASTNAME }, },
}
Minmap.mapPinGutters =
{
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_OBJECTIVE,			slice="FlagNeutral",				scale=1.0,	pins={ SystemData.MapPips.OBJECTIVE }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_KEEP,				slice="Keep-Grayed",				scale=0.75,	pins={ SystemData.MapPips.KEEP }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_GROUP_MEMBER,		slice="PlayerCircleGroupmate",		scale=1.75,	pins={ SystemData.MapPips.GROUP_MEMBER }, },
	{ label=StringTables.MapPinFilterNames.FILTER_TYPE_IMPORTANT_MONSTER,	slice="BombNeutral",				scale=1.0,	pins={ SystemData.MapPips.IMPORTANT_MONSTER }, },
}


function Minmap.Initialize()
	if(not MinmapSettings)then
		MinmapSettings = {}
		for k,v in pairs(defaultsettings) do
			MinmapSettings[k] = v
		end
	end

	if(MinmapSettings.ver < defaultsettings.ver)then
		for k,v in pairs(defaultsettings) do
			if (not MinmapSettings[k])
				then MinmapSettings[k] = v
			end
		end
	end
	MinmapSettings.ver=defaultsettings.ver

	CreateWindow(windowname, true)
	CreateWindow(filtername, false)

	LayoutEditor.RegisterWindow(windowname,
								 GetStringFromTable( "HUDStrings", StringTables.HUD.LABEL_HUD_EDIT_MINI_MAP_NAME ),
								 GetStringFromTable( "HUDStrings", StringTables.HUD.LABEL_HUD_EDIT_MINI_MAP_DESC ),
								 false, false,
								 true, nil )

	WindowRegisterEventHandler(windowname, SystemData.Events.LOADING_END,							"Minmap.UpdateMap")
	WindowRegisterEventHandler(windowname, SystemData.Events.PLAYER_ZONE_CHANGED,		 			"Minmap.OnZoneChange")
	WindowRegisterEventHandler(windowname, SystemData.Events.SCENARIO_BEGIN,				 		"Minmap.ScenarioBegin")
	WindowRegisterEventHandler(windowname, SystemData.Events.SCENARIO_END,							"Minmap.UpdateScenarioButtons")
	WindowRegisterEventHandler(windowname, SystemData.Events.PLAYER_RVR_FLAG_UPDATED,				"Minmap.UpdateScenarioButtons")
	WindowRegisterEventHandler(windowname, SystemData.Events.INTERACT_UPDATED_SCENARIO_QUEUE_LIST,	"Minmap.UpdateScenarioButtons")
	WindowRegisterEventHandler(windowname, SystemData.Events.SCENARIO_ACTIVE_QUEUE_UPDATED,			"Minmap.UpdateScenarioQueueButton")
	WindowRegisterEventHandler(windowname, SystemData.Events.TOGGLE_WORLD_MAP_WINDOW,				"Minmap.ToggleWorldMapWindow")
	WindowRegisterEventHandler(windowname, SystemData.Events.PLAYER_AREA_NAME_CHANGED,				"Minmap.OnNameChange" )
	WindowRegisterEventHandler(windowname, SystemData.Events.MAILBOX_UNREAD_COUNT_CHANGED, 			"Minmap.UpdateMailIcon")
	WindowRegisterEventHandler(windowname, SystemData.Events.RALLY_CALL_INVITE, 					"Minmap.ActivateRallyCall")
	LayoutEditor.RegisterEditCallback(Minmap.LayoutEditorDone)

	for index, filterType in pairs( SystemData.MapPips )
	do
		if( MinmapSettings.mapPinFilters[filterType] == nil ) then
			MinmapSettings.mapPinFilters[filterType] = true
		end
	end

	Minmap.InitMap(SystemData.MapTypes.OVERHEAD)
	Minmap.OnZoneChange()
	Minmap.UpdateScenarioButtons()
	Minmap.UpdateScenarioQueueButton()
	Minmap.RefreshMapPointFilterMenu()
	Minmap.UpdateMailIcon()

	zoomlevelvar=MinmapSettings.zoomlevel
	Minmap.SlideZoom(zoomlevelvar)
	EA_Window_OverheadMap.ToggleWorldMapWindow = function() return end
end


function Minmap.InitMap(maptype,zlv)
	Minmap.UpdateMapScale(true,zlv)
	RemoveMapInstance(map_windowname)
	CreateMapInstance(map_windowname,maptype)

	for filterType,show in pairs(MinmapSettings.mapPinFilters)
	do
		MapSetPinFilter(map_windowname,filterType,show)
	end
	for filterType,show in pairs(MinmapSettings.mapPinGutters)
	do
		MapSetPinGutter(map_windowname,filterType,show)
	end

	Minmap.UpdateMap()
end

function Minmap.UpdateMapScale(init,zlv)
	if((zlv or zoomlevelvar)>1)
	then
		WindowSetScale(map_windowname,WindowGetScale(windowname)*MinmapSettings.worldmapscale)
		if(not init)
		then
			Minmap.InitMap(SystemData.MapTypes.NORMAL,zlv)
		end
	else
		WindowSetScale(map_windowname,WindowGetScale(windowname)*MinmapSettings.mapscale)
		if(not init)
		then
			Minmap.InitMap(SystemData.MapTypes.OVERHEAD,zlv)
		end
	end
	WindowForceProcessAnchors(windowname)
end

function Minmap.OnNameChange()
	local text
	if(GameData.Player.isInSiege) then
		local cityId=GameData.Player.zone
		if(cityId==167)
		then
			text=L"IC "..GetCityInstanceId().instanceId
		elseif(cityId==168)
		then
			text=L"AD "..GetCityInstanceId().instanceId
		end
	else
		text = GameData.Player.area.name
		if(not text or text == L"") then
			text = GetStringFormatFromTable("MapSystem", StringTables.MapSystem.LABEL_ZONE_NAME, {GetZoneName(GameData.Player.zone)} )
		end

		if(not text or text == L"") then
			text = GetStringFormatFromTable("MapSystem", StringTables.MapSystem.LABEL_ZONE_NAME, {L"Zone "..GameData.Player.zone} )
		end
	end
	LabelSetText(windowname.."NameText", text or L"")
end

function Minmap.UpdateMap()
	MapSetMapView(map_windowname, GameDefs.MapLevel.ZONE_MAP, GameData.Player.zone)
end

function Minmap.Shutdown()
	MinmapSettings.zoomlevel=zoomlevelvar
	RemoveMapInstance(map_windowname)
end

function Minmap.LayoutEditorDone(event)
	if(LayoutEditor.EDITING_END~=event)then return end
	Minmap.UpdateMapScale()
end

function Minmap.OnMouseOverPoint()
	Tooltips.CreateMapPointTooltip(map_windowname, MinmapMapDisplay.MouseoverPoints, Tooltips.ANCHOR_CURSOR_LEFT)
end

function Minmap.OnClickMap()
	MapUtils.ClickMap(map_windowname, MinmapMapDisplay.MouseoverPoints)
end

function Minmap.ActivateRallyCall()
	WindowRegisterCoreEventHandler(windowname, "OnUpdate", "Minmap.RallyCallUpdate")

	Minmap.rallystart	= GetComputerTime()
	Minmap.rallyend		= (GetComputerTime()+120)%86400

	SystemData.AlertText.VecType = {2,16}
	SystemData.AlertText.VecText = {L"Rally Call!", L"Two Minutes Left!"}
	AlertTextWindow.AddAlert()
	ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.NORMAL, "EA_Texture_Menu01", "PaperDoll-Button")
	ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.HIGHLIGHTED, "EA_Texture_Menu01", "PaperDoll-Button")
	ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.PRESSED, "EA_Texture_Menu01", "PaperDoll-Button")
	ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.PRESSED_HIGHLIGHTED, "EA_Texture_Menu01", "PaperDoll-Button")
end

local oldtime = GetComputerTime()
function Minmap.RallyCallUpdate()
	if(GetComputerTime() - oldtime > 1)then
		local curtime = GetComputerTime()

		if(curtime < Minmap.rallyend)then
			WindowSetTintColor(windowname.."MapScenarioQueue", 0, 255, 255)
			if(Minmap.rallyend - curtime == 60)then
				SystemData.AlertText.VecType = {2,16}
				SystemData.AlertText.VecText = {L"Rally Call!", L"One Minute Left!"}
				AlertTextWindow.AddAlert()
			end
		else
			WindowUnregisterCoreEventHandler(windowname, "OnUpdate")
			Minmap.rallystart=nil
			Minmap.rallyend=nil
			ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.NORMAL, "EA_HUD_01", "RvR-Flag")
			ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.HIGHLIGHTED, "EA_HUD_01", "RvR-Flag")
			ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.PRESSED, "EA_HUD_01", "RvR-Flag")
			ButtonSetTextureSlice("MinmapMapScenarioQueue", Button.ButtonState.PRESSED_HIGHLIGHTED, "EA_HUD_01", "RvR-Flag")
			Minmap.UpdateScenarioButtons()
		end
		oldtime = GetComputerTime()
	else
		return
	end
end


function Minmap.OnZoneChange()
	Minmap.UpdateMap()
	Minmap.UpdateCityRating()
	Minmap.OnNameChange()
end

function Minmap.HandleMouseWheel(x, y, delta, flags)
	if (delta < 0) then
		Minmap.ZoomOut()
	elseif (delta > 0) then
		Minmap.ZoomIn()
	end
end

function Minmap.ZoomIn()
	local sliderFraction = zoomlevelvar

	local oneTickAmount = (1 / (SystemData.OverheadMap.MAX_ZOOM_LEVEL - SystemData.OverheadMap.MIN_ZOOM_LEVEL))

	if ((sliderFraction - oneTickAmount) >= 0) then
		sliderFraction = sliderFraction - oneTickAmount
	else
		sliderFraction = 0
	end

	if(zoomlevelvar~=sliderFraction)
	then
		Minmap.SlideZoom(sliderFraction)
		zoomlevelvar=sliderFraction
	end
end

function Minmap.ZoomOut()
	local sliderFraction = zoomlevelvar

	local oneTickAmount = (1 / (SystemData.OverheadMap.MAX_ZOOM_LEVEL - SystemData.OverheadMap.MIN_ZOOM_LEVEL))

	if ((sliderFraction + oneTickAmount) <= 1) then
		sliderFraction = sliderFraction + oneTickAmount
	else
		sliderFraction = 1+(oneTickAmount*MinmapSettings.worldzoom)
	end

	if(zoomlevelvar~=sliderFraction)
	then
		Minmap.SlideZoom(sliderFraction)
		zoomlevelvar=sliderFraction
	end
end

function Minmap.SlideZoom(zlv)
	local sliderFraction = 1.0 - zlv
	if(sliderFraction<0)
	then
		Minmap.InitMap(SystemData.MapTypes.NORMAL,zlv)
	else
		if(zoomlevelvar>1)
		then
			Minmap.InitMap(SystemData.MapTypes.OVERHEAD,zlv)
		end
		SetOverheadMapZoomLevel((sliderFraction * (SystemData.OverheadMap.MAX_ZOOM_LEVEL - SystemData.OverheadMap.MIN_ZOOM_LEVEL)) + SystemData.OverheadMap.MIN_ZOOM_LEVEL)
	end
	MinmapSettings.zoomlevel = zlv
end

function Minmap.UpdateCityRating()
	local cityId=GameDefs.ZoneCityIds[GameData.Player.zone]

	-- Is nil if the zone is not a city zone
	if(cityId == nil) then
		WindowSetShowing(windowname.."CityRating", false)
		return
	end

	MapUtils.UpdateCityRatingWindow(cityId, windowname.."CityRating")
	WindowSetShowing(windowname.."CityRating", true)
end

function Minmap.OnMouseOverCityRating()
	local cityId = GameDefs.ZoneCityIds[GameData.Player.zone]
	if(cityId == nil) then
		return
	end
	local cityRating = GetCityRatingForCityId( cityId )
	local titleText = GetStringFromTable("RvRCityStrings", StringTables.RvRCity.LABEL_CITY_RANK)
	local ratingText = L""
	local descStringId = StringTables.RvRCity[ "CITY_"..cityId.."_RATING_"..cityRating.."_DESC" ]
	if(descStringId ~= nil) then
		ratingText = GetStringFromTable( "RvRCityStrings", descStringId )
	end

	local itemsText = {}

	for rating = Tooltips.NUM_CITY_RANKS, 1, -1 do
		if( rating <= cityRating ) then
			local activityRankIndex	= 1
			local descItemStringId	= StringTables.RvRCity[ "CITY_"..cityId.."_RATING_"..rating.."_ACTIVITY_"..activityRankIndex ]
			while(descItemStringId) do
				local text = GetStringFromTable( "RvRCityStrings", descItemStringId )
				table.insert( itemsText, text )
				activityRankIndex	= activityRankIndex + 1
				descItemStringId	= StringTables.RvRCity[ "CITY_"..cityId.."_RATING_"..rating.."_ACTIVITY_"..activityRankIndex ]
			end
		end
	end

	local text = GetStringFormatFromTable( "RvRCityStrings", StringTables.RvRCity.RANK_X_CITY_AND_PQ_AVAIL, { L""..cityRating } )
	table.insert( itemsText, text )

	Tooltips.CreateListTooltip(titleText, ratingText, itemsText, SystemData.ActiveWindow.name, Tooltips.ANCHOR_WINDOW_LEFT)
end

function Minmap.ToggleWorldMapWindow()
	WindowUtils.ToggleShowing("EA_Window_WorldMap")
end

function Minmap.OnLButtonDown()
end

function Minmap.UpdateMailIcon()
	local showIcon = (GameData.Mailbox.PLAYER.unreadCount + GameData.Mailbox.AUCTION.unreadCount) > 0

	if (showIcon ~= WindowGetShowing(windowname.."MailNotificationIcon")) then
		WindowSetShowing(windowname.."MailNotificationIcon", showIcon)
	end
end

function Minmap.OnMouseoverMailIcon()
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )

	Tooltips.SetTooltipText( 1, 1, L"Unread Mail: "..GameData.Mailbox.PLAYER.unreadCount )
	Tooltips.SetTooltipText( 2, 2, L"Unread Auctions: "..GameData.Mailbox.AUCTION.unreadCount )

	Tooltips.Finalize()
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)
end

----------------------------------------------------------------
-- Scenario Queue
----------------------------------------------------------------

function Minmap.OnScenarioQueueLButtonUp()
	Minmap.OnJoinAScenario()
end

function Minmap.OnJoinAScenario()
	BroadcastEvent(SystemData.Events.INTERACT_SHOW_SCENARIO_QUEUE_LIST)
end

function Minmap.OnScenarioQueueRButtonUp()
end

function Minmap.OnMouseoverScenarioQueue()
	local scenarioQueueData = GetScenarioQueueData()
	local row = 1
	local column = 1

	if(LeavingQueues)
	then
		Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
		Tooltips.SetTooltipText( row, column, L"Leaving scenarios ..." )
		Tooltips.Finalize()
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)
	else
		if(scenarioQueueData ~= nil)
		then
			local totalScenarios = scenarioQueueData.totalQueuedScenarios
			local text,scenarioName,data
			EA_Window_ContextMenu.CreateContextMenu( SystemData.ActiveWindow.name)
			for index = 1, totalScenarios do
				data=scenarioQueueData[index]
				if(data.type == Minmap.INSTANCETYPE_SCENARIO) then
					scenarioName=GetScenarioName(data.id)
				else
					if(data.id~=0)
					then
						scenarioName=GetStringFormat(StringTables.Default.TEXT_CITY_INSTANCE_QUEUE_SPECIFIC,{data.id})
					else
						scenarioName=GetString(StringTables.Default.TEXT_CITY_INSTANCE_QUEUE_GENERAL)
					end
				end
				text=GetStringFormat(StringTables.Default.TEXT_LEAVE_SCENARIO,{scenarioName})
				EA_Window_ContextMenu.AddMenuItem(text,Minmap.LeaveScenario,false,true)
			end
			if(totalScenarios>1)then EA_Window_ContextMenu.AddMenuItem(L"Leave all Scenarios Queues",Minmap.LeaveAllScenarios,false,true) end
			local x,y=WindowGetOffsetFromParent(windowname)
			if(y>SystemData.screenResolution.y)
			then
				EA_Window_ContextMenu.Finalize(nil,{["RelativeTo"]=windowname,["Point"]="topleft",["RelativePoint"]="bottomleft",["XOffset"]=0,["YOffset"]=-3})
			else
				EA_Window_ContextMenu.Finalize(nil,{["RelativeTo"]=windowname,["Point"]="topleft",["RelativePoint"]="topleft",["XOffset"]=3,["YOffset"]=51})
			end
		else
			Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
			if(GameData.ScenarioQueueData[1].id==0)
			then
				Tooltips.SetTooltipText( row, column, L"Left click to queue for scenarios (none aviable at the moment)" )
			else
				Tooltips.SetTooltipText( row, column, L"Left click to queue for scenarios" )
			end
			Tooltips.SetTooltipText( row+1, column, L"Right click to set map filters" )
			Tooltips.SetTooltipText( row+2, column, L"Middle click to open the war report or answer rally calls")
			Tooltips.Finalize()
			Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)
		end
	end
end

local function ARC()
	BroadcastEvent(SystemData.Events.RALLY_CALL_JOIN)
end
local function OWR()
	EA_Window_CurrentEvents.SetCurrentTier(Player.GetTier())
	WindowUtils.ToggleShowing("EA_Window_CurrentEvents")
end

function Minmap.AnswerCall()
	if(Minmap.rallyend~=nil)
	then
		DialogManager.MakeTwoButtonDialog(L"Answer Rally Call or Open WAR Report ?",L"Rally Call",ARC,L"WAR Report",OWR,10)
	else
		OWR()
	end
end

function Minmap.UpdateScenarioQueueButton()
	local queuedScenarioData = GetScenarioQueueData()

	if( queuedScenarioData ~= nil) then
		WindowStartAlphaAnimation( windowname.."MapScenarioQueue", Window.AnimationType.LOOP, 0.1, 1.0, 0.5, false, 0, 0 )
	else
		WindowStopAlphaAnimation(windowname.."MapScenarioQueue")

		--The scenario window may not exist yet...
		if( DoesWindowExist("EA_Window_InScenarioQueue") ) then
			WindowSetShowing( "EA_Window_InScenarioQueue", false )
		end
	end
end

----------------------------------------------------------------
-- Scenario Summary
----------------------------------------------------------------

function Minmap.ToggleScenarioGroupWindow()
	if( GameData.Player.isInScenario or GameData.Player.isInSiege )
	then
		WindowSetShowing("ScenarioGroupWindow",not WindowGetShowing("ScenarioGroupWindow"))
	end
end

function Minmap.OnMouseoverScenarioSummaryBtn()

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )

	local row = 1
	local column = 1
	Tooltips.SetTooltipText( row, column, L"Left click for summary" )

	column = column + 1
	Tooltips.SetTooltipColor( row, column, 140, 100, 0 )
	Tooltips.SetTooltipText( row, column, L"("..KeyUtils.GetFirstBindingNameForAction("TOGGLE_SCENARIO_SUMMARY_WINDOW")..L")" )

	row = row + 1
	Tooltips.SetTooltipText( row, column - 1, L"Right click for group window" )

	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT)
end

function Minmap.OnMouseoverScenarioGroupBtn()

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )

	local row = 1
	local column = 1
	Tooltips.SetTooltipText( row, column, GetString( StringTables.Default.LABEL_SCENARIO_GROUPS ) )

	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT)
end

function Minmap.ScenarioBegin()
	WindowSetShowing(filtername,false)
	Minmap.UpdateScenarioButtons()
end

function Minmap.UpdateScenarioButtons()
	-- Only show the buttons when map is available
	local instancemode=GameData.Player.isInScenario or GameData.Player.isInSiege
	WindowSetShowing(windowname.."ScenarioSummaryButton",instancemode)
	WindowSetShowing(windowname.."MapScenarioQueue",not instancemode)

	if(Minmap.rallystart==nil)
	then
		if(GameData.ScenarioQueueData[1].id==0)
		then
			WindowSetTintColor(windowname.."MapScenarioQueue",150,0,0)
		else
			WindowSetTintColor(windowname.."MapScenarioQueue",255,0,0)
		end
	end
end

function Minmap.LeaveAllScenarios()
	LeavingQueues=true
	WindowRegisterCoreEventHandler(windowname.."MapScenarioQueue","OnUpdate","Minmap.LeaveNextScenarios")
	WindowStopAlphaAnimation(windowname.."MapScenarioQueue")
	WindowSetTintColor(windowname.."MapScenarioQueue",150,0,0)
end

function Minmap.LeaveNextScenarios()
	local scenrioQueueData = GetScenarioQueueData()

	if(scenrioQueueData~=nil)
	then
		if(scenrioQueueData[1].type == Minmap.INSTANCETYPE_SCENARIO)
		then
			GameData.ScenarioQueueData.selectedId = scenrioQueueData[1].id
			BroadcastEvent(SystemData.Events.INTERACT_LEAVE_SCENARIO_QUEUE)
		else
			BroadcastEvent(SystemData.Events.CITY_CAPTURE_LEAVE_QUEUE)
		end
	else
		LeavingQueues=false
		WindowUnregisterCoreEventHandler(windowname.."MapScenarioQueue", "OnUpdate")
		Minmap.UpdateScenarioQueueButton()
		Minmap.UpdateScenarioButtons()
	end
end

function Minmap.LeaveScenario()
	local scenrioQueueData = GetScenarioQueueData()
	local windowId = WindowGetId(SystemData.ActiveWindow.name)

	if(scenrioQueueData~=nil and scenrioQueueData[windowId]~=nil)
	then
		if( scenrioQueueData[windowId].type == Minmap.INSTANCETYPE_SCENARIO ) then
			GameData.ScenarioQueueData.selectedId = scenrioQueueData[windowId].id
			BroadcastEvent( SystemData.Events.INTERACT_LEAVE_SCENARIO_QUEUE )
		else
			BroadcastEvent( SystemData.Events.CITY_CAPTURE_LEAVE_QUEUE )
		end
	end
end

----------------------------------------------------------------
-- Map Point Filter Menu
----------------------------------------------------------------
local function setdisplayorder(listtype)
	local displayOrder = {}
	for index, _ in ipairs(Minmap["mapPin"..listtype.."s"]) do
		table.insert(displayOrder, index)
	end
	ListBoxSetDisplayOrder(filtername..listtype.."List", displayOrder)
end

function Minmap.RefreshMapPointFilterMenu()
	-- Display everything
	setdisplayorder("Filter")
	setdisplayorder("Gutter")
	LabelSetText(filtername.."Filter".."Heading",GetStringFromTable("MapSystem",StringTables.MapSystem.LABEL_MAP_FILTERS))
	LabelSetText(filtername.."Gutter".."Heading",GetStringFromTable("MapSystem",StringTables.MapSystem.LABEL_MAP_GUTTERS))
	LabelSetText(filtername.."_ScaleText",L"Mapmarker Scale")
	LabelSetText(filtername.."_ScaleSliderText",L"Map")
	LabelSetText(filtername.."_WorldScaleSliderText",L"Worldmap")
	LabelSetText(filtername.."_WorldZoomCheckBoxText",L"Worldmap Zoom")
end

function Minmap.PopulateFilter()
	Minmap.PopulateList(MinmapPinMenuFilterList,"Filter")
end

function Minmap.PopulateGutter()
	Minmap.PopulateList(MinmapPinMenuGutterList,"Gutter")
	SliderBarSetCurrentPosition(filtername.."_ScaleSlider",MinmapSettings.mapscale-ScaleOffset)
	SliderBarSetCurrentPosition(filtername.."_WorldScaleSlider",MinmapSettings.worldmapscale-ScaleOffset)
	local worldscale_disable=MinmapSettings.worldzoom~=1
	local color=worldscale_disable and 150 or 255
	SliderBarSetDisabledFlag(filtername.."_WorldScaleSlider",worldscale_disable)
	WindowSetTintColor(filtername.."_WorldScaleSlider",color,color,color)
	ButtonSetPressedFlag(filtername.."_WorldZoomCheckBoxButton",MinmapSettings.worldzoom==1)
end

function Minmap.PopulateList(list,listtype)
	local dataindex="mapPin"..listtype.."s"
	local listdata=Minmap[dataindex]
	local pindata=MinmapSettings[dataindex]
	local baserowname=filtername..listtype.."ListRow"

	for row, data in ipairs(list.PopulatorIndices) do
		local filterData	= listdata[data]
		local rowFrame		= baserowname..row
		local buttonFrame	= rowFrame.."Button"
		local iconFrame		= rowFrame.."Icon"
		local labelFrame	= rowFrame.."Label"

		-- Label the button
		LabelSetText(labelFrame, GetStringFromTable("MapPointFilterNames", filterData.label))

		-- Set up the check button state
		ButtonSetCheckButtonFlag(buttonFrame, true)

		local enableButton = false

		for _, pinType in ipairs(filterData.pins) do
			if pindata[pinType] then
				enableButton = true
				break
			end
		end

		ButtonSetPressedFlag(buttonFrame, enableButton)

		-- Add the appropriate map icon
		DynamicImageSetTextureScale(iconFrame, filterData.scale)
		DynamicImageSetTextureSlice(iconFrame, filterData.slice)
	end
end

function Minmap.ToggleFilterMenu()
	if(Fader and Fader.version>=1.1)
	then
		Fader.FadeTo(filtername,not WindowGetShowing(filtername) and 1 or 0)
	else
		WindowUtils.ToggleShowing(filtername)
	end
end

function Minmap.OnMouseOverFilterMenuButton()
	Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name,GetStringFromTable("MapSystem",StringTables.MapSystem.LABEL_MAP_FILTERS_BUTTON))
	Tooltips.Finalize()
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)
end

function Minmap.ToggleMapPinGutter()
	Minmap.ToggleMapPin("Gutter")
end

function Minmap.ToggleMapPinFilter()
	Minmap.ToggleMapPin("Filter")
end

function Minmap.ToggleMapPin(listtype)
	local window=SystemData.ActiveWindow.name
	local filterIndex = WindowGetId(window)
	local showPin = ButtonGetPressedFlag(window.."Button")
	local dataindex="mapPin"..listtype.."s"
	local pinTypes = Minmap[dataindex][filterIndex].pins
	local MapSetPin=_G["MapSetPin"..listtype]

	for _, pinType in ipairs(pinTypes) do
		-- Update the Settings
		MapSetPin(map_windowname, pinType, showPin)
		MinmapSettings[dataindex][pinType] = showPin
	end
end

function Minmap.ScaleSliderChanged(pos)
	MinmapSettings.mapscale=pos+ScaleOffset
	if(zoomlevelvar<=1)
	then
		Minmap.UpdateMapScale()
	end
end

function Minmap.WorldScaleSliderChanged(pos)
	if(SliderBarGetDisabledFlag(filtername.."_WorldScaleSlider"))then return end

	MinmapSettings.worldmapscale=pos+ScaleOffset
	if(zoomlevelvar>1)
	then
		Minmap.UpdateMapScale()
	end
end

function Minmap.ToggleWorldMapZoom()
	local actwnd		= SystemData.ActiveWindow.name
	local buttonName	= actwnd.."Button"

	if( ButtonGetDisabledFlag( buttonName ) ) then
		return
	end

	local pressed = not ButtonGetPressedFlag(buttonName)
	ButtonSetPressedFlag(buttonName,pressed)

	if(pressed)
	then
		MinmapSettings.worldzoom=1
	else
		MinmapSettings.worldzoom=0
		if(zoomlevelvar>1)
		then
			Minmap.ZoomOut()
		end
	end
	local worldscale_disable=MinmapSettings.worldzoom~=1
	local color=worldscale_disable and 150 or 255
	SliderBarSetDisabledFlag(filtername.."_WorldScaleSlider",worldscale_disable)
	WindowSetTintColor(filtername.."_WorldScaleSlider",color,color,color)
end
